import { useState, Fragment } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import FormHelperText from '@mui/material/FormHelperText';
import Box from '@mui/material/Box';

import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineDot from '@mui/lab/TimelineDot';
import TimelineOppositeContent from '@mui/lab/TimelineOppositeContent';

import LocationOnIcon from '@mui/icons-material/LocationOn';
import EventIcon from '@mui/icons-material/Event';

import enLocale from 'date-fns/locale/en-GB';

import './App.scss';

import version from './version';

import success from './assets/undraw_authentication_re_svpt.svg'
import fail from './assets/undraw_cancel_re_ctke.svg'

function App() {

  let date = new Date()
  let today = date.toISOString().split('T')[0]
  const daysAfterLastDose = 14
  const daysToElligible = 152 - daysAfterLastDose
  const daysToLapse = 270 - daysAfterLastDose

  const [effectiveDoseDate, setEffectiveDoseDate] = useState<Date | null>(date)
  const [eligibleBoosterDate, setEligibleBoosterDate] = useState<Date | null>(null)
  const [eligibleBoosterDays, setEligibleBoosterDays] = useState<Number | null>(null)
  const [vaccineLapsedDate, setVaccineLapsedDate] = useState<Date | null>(null)
  const [vaccineLapsedDays, setVaccineLapsedDays] = useState<Number | null>(null)

  function handleEffectiveDoseDateChange(value: any) {
    console.log(value)
    setEffectiveDoseDate(value)
    calculateBoosterDate(value)
  }

  function vaccineLapsed() {
    return (vaccineLapsedDays !== null && vaccineLapsedDays >= 1)
  }

  function addDays(fromDate: any, days: number) {
    var date = new Date(fromDate)
    date.setDate(date.getDate() + days)
    return date
  }

  function diffDays(fromDate: string, toDate: string) {
    const _fromDate = new Date(fromDate)
    const _toDate = new Date(toDate)
    const _diffTime = _toDate.getTime() - _fromDate.getTime()
    const _diffDays = Math.ceil(_diffTime / (1000 * 60 * 60 * 24))
    return _diffDays
  }

  function calculateBoosterDate(effectDate: any) {
    /*
      D0 = Last Dose
      D+14 = Effective Date (Considered Vaccinated)
      D+152 = (~5months) Elligible for booster
      D+270 = unvaccinated status effective
    */

    var _effectiveDate = new Date(effectDate)
    var _eligibleBoosterDate = addDays(_effectiveDate, daysToElligible)
    var _lapseVaccineDate = addDays(_effectiveDate, daysToLapse)

    console.log('calculateBoosterDate', {
      'effectiveDate': _effectiveDate.toDateString(),
      'eligibleBoosterDate': _eligibleBoosterDate.toDateString(),
      'lapseBoosterDate': _lapseVaccineDate.toDateString()
    })

    let _daysToLapsed = diffDays(today, _lapseVaccineDate.toDateString())
    let _daysToElligible = diffDays(today, _eligibleBoosterDate.toDateString())

    setVaccineLapsedDays(_daysToLapsed)
    setEligibleBoosterDays(_daysToElligible)

    setEligibleBoosterDate(_eligibleBoosterDate)
    setVaccineLapsedDate(_lapseVaccineDate)


  }

  function getMonth(n: number) {
    return ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][n]
  }

  function toDDMMMYYYY(d: Date) {
    var dd = String(d.getDate()).padStart(2, '0');
    // var mm = String(d.getMonth() + 1).padStart(2, '0'); //January is 0!
    var mmm = getMonth(d.getMonth());
    var yyyy = d.getFullYear();

    return `${dd} ${mmm} ${yyyy}`
  }

  return (
    <div className="app">
      <div className={"main"}>
        <Stack spacing={2}>
          <Box>
            <img src="/vaccinegowhere--logo-square.svg" height="52px" alt="logo" />
          </Box>
          <Box sx={{ maxWidth: 'sm', padding: '.5em' }}>
            <Typography variant="body2" display="block" color="text.secondary">{'DISCLAIMER'}</Typography>
            <Typography variant="caption" display="block" color="text.secondary">
              {'The information and other material contained on this website are for informational purposes only. It is not intended to be a substitute for professional medical advice, diagnosis or treatment.'}
            </Typography>
          </Box>
          <Box sx={{ maxWidth: 'sm', padding: '.5em' }}>
            <Typography variant="caption" component="div" color={'#D50000'} gutterBottom>
              {'This site will no longer be maintained as it is now been superseeded by the latest TraceTogether version that allows you to view your vaccination status expiry!'}
            </Typography>
          </Box>
          <Card sx={{ maxWidth: 'sm' }}>
            <CardContent>

              <Typography variant="h5" gutterBottom component="div">
                {'Check when to take your booster'}
              </Typography>

              <LocalizationProvider dateAdapter={AdapterDateFns} locale={enLocale}>
                <DatePicker
                  disableFuture
                  label="Vaccine Effective Date"
                  value={effectiveDoseDate}
                  onChange={handleEffectiveDoseDateChange}
                  renderInput={(params) => <TextField {...params} />}
                />
              </LocalizationProvider>
              <FormHelperText>{'This date is 14 days from your last dose, this website also assumes you have taken a series of 2 MRNA OR 3 Sinovac shot and waiting for your booster.'}</FormHelperText>

            </CardContent>
          </Card>

          {vaccineLapsedDate !== null && vaccineLapsedDays !== null &&
            <Card sx={{ maxWidth: 'sm' }}>
              <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                  <CardContent>
                    {vaccineLapsed() &&
                      <Fragment>
                        <Typography variant="subtitle1" color="text.secondary">
                          {'Vaccinated days left'}
                        </Typography>
                        {vaccineLapsedDays &&
                          <Typography variant="h3" component="div" color={(vaccineLapsedDays > 14) ? "primary" : "#D50000"} >
                            {vaccineLapsedDays}
                          </Typography>
                        }
                        {vaccineLapsedDays &&
                          <Typography variant="caption" component="div" color={'text.secondary'} gutterBottom>
                            {(vaccineLapsedDays > 14) ?
                              'It is recommended you take a booster within 5 months from your last dose.' : 'Your status may lapse as it falls below the 14 days requirement for the booster to be effective'}
                          </Typography>
                        }
                      </Fragment>
                    }
                    {!vaccineLapsed() &&
                      <Fragment>
                        <Typography variant="h5" gutterBottom>
                          {'You are now unvaccinated!'}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                          {'It\'s been more than 270 days after your last shot'}
                        </Typography>
                      </Fragment>
                    }
                  </CardContent>
                </Box>
                <CardMedia
                  component="img"
                  sx={{ width: 'unset', height: '100px', m: '16px' }}
                  image={vaccineLapsed() ? success : fail}
                  alt={vaccineLapsed() ? "Illustration showing a check mark indicating a positive remark" : "Illustration showing a cross indicating a negative remark"}
                />

              </Box>

              {(eligibleBoosterDays && eligibleBoosterDays < 1) &&
                <CardActions>
                  <Button variant="contained" target="_blank" href="https://appointment.vaccine.gov.sg" startIcon={<EventIcon />}>{'Book appointment'}</Button>
                  <Button target="_blank" href="https://vaccinegowhere.sg" startIcon={<LocationOnIcon />}>{'Find Clinics'}</Button>
                </CardActions>
              }
            </Card>
          }

          {vaccineLapsedDate !== null && vaccineLapsedDays !== null &&
            <Card sx={{ maxWidth: 'sm' }}>
              <CardContent>
                <Typography variant="h5">{'Timeline'}</Typography>
              </CardContent>
              <Timeline>
                <TimelineItem>
                  <TimelineOppositeContent color="text.secondary">
                    {effectiveDoseDate && toDDMMMYYYY(effectiveDoseDate)}
                  </TimelineOppositeContent>
                  <TimelineSeparator>
                    <TimelineDot color="primary" />
                    <TimelineConnector color="primary" />
                  </TimelineSeparator>
                  <TimelineContent>Vaccine Effective</TimelineContent>
                </TimelineItem>
                {eligibleBoosterDate &&
                  <TimelineItem>
                    <TimelineOppositeContent color="text.secondary">
                      {toDDMMMYYYY(eligibleBoosterDate)}
                    </TimelineOppositeContent>
                    <TimelineSeparator>
                      <TimelineDot variant={(diffDays(today, eligibleBoosterDate.toDateString()) > 0) ? "outlined" : "filled"} color="primary" />
                      <TimelineConnector color="primary" />
                    </TimelineSeparator>
                    <TimelineContent>Eligible for Booster</TimelineContent>
                  </TimelineItem>
                }
                <TimelineItem>
                  <TimelineOppositeContent color="text.secondary">
                    {vaccineLapsedDate && toDDMMMYYYY(vaccineLapsedDate)}
                  </TimelineOppositeContent>
                  <TimelineSeparator>
                    <TimelineDot variant={(diffDays(today, vaccineLapsedDate.toDateString()) > 0) ? "outlined" : "filled"} color="primary" />
                  </TimelineSeparator>
                  <TimelineContent>Unvaccinated</TimelineContent>
                </TimelineItem>
              </Timeline>
            </Card>
          }
          <Card sx={{ maxWidth: 'sm' }}>
            <CardContent>
              <Typography variant="body2" gutterBottom component="div">
                {'MOH and the Expert Committee on COVID-19 Vaccination (EC19V) recommends that all persons aged 12 years and above should receive a booster dose of an mRNA* vaccine offered under the National Vaccination Programme 5 months after completing their primary vaccination series.'}
              </Typography>
              <Typography variant="caption" display="block" color="text.secondary">
                {'* Those age 18 and above are recommended to get the Pfizer-BioNTech/Comirnaty or the Moderna vaccine, 12 - 17 years old are recommended to get the Pfizer-BioNTech/Comirnaty vaccine. '}<Link href="https://www.gov.sg/article/accelerating-booster-vaccination-programme" target="_blank">Source</Link>
              </Typography>
            </CardContent>
            <CardActions>
              <Button href="https://www.moh.gov.sg/covid-19/vaccination/faqs---booster-doses" target="_blank" >{'READ MORE'}</Button>
            </CardActions>
          </Card>
          <footer>
            <Box sx={{
              m: 1,
              maxWidth: 'sm'
            }}>
              <Stack spacing={2}>
                <Box>
                  <Typography variant="body2" display="block" color="text.secondary" gutterBottom>{'NOTICE OF NON-AFFILIATION AND DISCLAIMER'}</Typography>
                  <Typography variant="caption" display="block" gutterBottom>
                    {'We are not affiliated, associated, authorized, endorsed by, or in any way officially connected with the Singapore Government, the Ministry of Health or any of its subsidiaries or its affiliates. The official COVID-19 Vaccine Website by the Ministry of Health can be found at '}<Link href="https://www.vaccine.gov.sg/">{'vaccine.gov.sg'}</Link>{'. '}<br /><Link href="https://vaccinegowhere.sg/disclaimer" target="_blank" rel="noopener nofollow" >{'Read Full Disclaimer'}</Link>
                  </Typography>
                  <Typography variant="caption" display="block" color="text.secondary" gutterBottom>
                    <Link href="https://forms.gle/a11uvu1CLMoeVFW57" target="_blank" rel="noopener nofollow">{'Report an issue.'}</Link>
                  </Typography>
                  <Typography variant="caption" display="block" color="text.secondary" gutterBottom>
                    {'Built by '}<Link href="https://jordanlys.com" target="_blank" rel="noopener nofollow">{'@jordanlys95'}</Link>. {'Open-sourced on '}<Link href="https://gitlab.com/jordanlys95/check-booster" target="_blank" rel="noopener nofollow">{'GitLab'}</Link>.
                  </Typography>
                  <Typography variant="caption" display="block" color="text.secondary" gutterBottom>
                    {'Build #' + version}
                  </Typography>
                </Box>
              </Stack>
            </Box>
          </footer>
        </Stack>
      </div>
    </div>
  );
}

export default App;
