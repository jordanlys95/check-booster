# Check Booster | VacccineGoWhere

> **NOTICE OF NON-AFFILIATION AND DISCLAIMER:**
> We are not affiliated, associated, authorized, endorsed by, or in any way officially connected with the Singapore Government, the Ministry of Health or any of its subsidiaries or its affiliates. The official COVID-19 Vaccine Website by the Ministry of Health can be found at https://www.vaccine.gov.sg/.
> 
> **MEDICAL DISCLAIMER: THIS WEBSITE DOES NOT PROVIDE MEDICAL ADVICE.**
> The information and other material contained on this website are for informational purposes only. The purpose of this website is to promote broad consumer understanding and knowledge of various vaccinations available. It is not intended to be a substitute for professional medical advice, diagnosis or treatment. Always seek the advice of your physician or other qualified health care provider with any questions you may have regarding a medical condition or treatment and before undertaking a new health care regimen, and never disregard professional medical advice or delay in seeking it because of something you have read on this website.
> 
> The vaccine names Pfizer a.k.a Pfizer-BioNTech and Moderna as well as related names, marks, emblems and images are registered trademarks of their respective owners.
